package net.wombyte;

public class Wombyte {
    private static boolean isValueAllowed(byte character) {
        return 32 <= character && character <= 126;
    }

    private static String filterValue(String value) {
        StringBuilder builder = new StringBuilder();

        for (byte ch : value.getBytes()) {
            if (isValueAllowed(ch)) {
                builder.append((char) ch);
            }
        }

        return builder.toString();
    }

    private static String stripValue(String value) {
        return value.length() > 128 ? value.substring(0, 128) : value;
    }

    public static String toValue(String value) {
        return stripValue(filterValue(value));
    }

    private static boolean isKeyAllowed(byte character) {
        return Character.isLetterOrDigit(character) || character == '_';
    }

    private static String filterField(String value) {
        StringBuilder builder = new StringBuilder();

        for (byte ch : value.getBytes()) {
            if (isKeyAllowed(ch)) {
                builder.append((char) ch);
            }
        }

        return builder.toString();
    }

    private static String stripField(String value) {
        return value.length() > 32 ? value.substring(0, 32) : value;
    }

    public static String toField(String value) {
        return stripField(filterField(
            value
                .replace(' ', '_')
                .replace('-', '_'))
        ).toLowerCase();
    }
}
