package net.wombyte.event;

import net.wombyte.Wombyte;

import java.util.Random;

public class WombyteEventContext {

    public final long timestamp;
    public final int id;
    public final String event;
    public final String app;

    public WombyteEventContext(String event, String app){
        this.timestamp = System.currentTimeMillis() / 1000;
        this.id = Math.abs(new Random().nextInt());
        this.event = Wombyte.toField(event);
        this.app = Wombyte.toField(app);
    }
}
