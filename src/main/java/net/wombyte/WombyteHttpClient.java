package net.wombyte;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WombyteHttpClient {
    private final String token;
    private final String secret;

    private final URL base;
    private final char[] buffer = new char[4096];

    public WombyteHttpClient(String host, String token, String secret) throws MalformedURLException {
        this.token = token;
        this.secret = secret;

        this.base = new URL("http://" + host + "/");
    }

    public int sendFile(File file) throws IOException {
        HttpURLConnection connection = getHttpURLConnection();

        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
             BufferedReader reader = new BufferedReader(new FileReader(file))) {

            int len = reader.read(buffer);

            while (len != -1) {
                writer.write(buffer, 0, len);
                len = reader.read(buffer);
            }

            writer.flush();

            return connection.getResponseCode();
        } finally {
            connection.disconnect();
        }
    }

    private HttpURLConnection getHttpURLConnection() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) base.openConnection();

        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Wombyte-Access-Token", token);
        connection.setRequestProperty("Wombyte-Access-Key", secret);

        return connection;
    }


}
