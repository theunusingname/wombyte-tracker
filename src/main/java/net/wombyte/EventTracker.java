package net.wombyte;

import java.io.Closeable;
import java.util.concurrent.CompletableFuture;

public interface EventTracker extends Closeable {
    Event track(String event);

    CompletableFuture flush();
}
