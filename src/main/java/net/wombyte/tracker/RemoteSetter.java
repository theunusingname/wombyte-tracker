package net.wombyte.tracker;

public interface RemoteSetter {
    TrackerBuilder setRemote(String host, String token, String secret);
}
