package net.wombyte.tracker;

import net.wombyte.EventTracker;
import net.wombyte.WombyteHttpClient;
import net.wombyte.buffer.WombyteBuffer;
import net.wombyte.buffer.WombyteBufferArgs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class WombyteTrackerBuilder implements PathSetter, RemoteSetter, TrackerBuilder {

    private static ConcurrentHashMap<WombyteBufferArgs, WombyteBuffer> buffers =
            new ConcurrentHashMap<>();

    private String directory;

    private String host;
    private String token;
    private String secret;

    public static PathSetter create() {
        return new WombyteTrackerBuilder();
    }

    private WombyteTrackerBuilder() {}

    @Override
    public RemoteSetter setLogDirectory(String path) {
        if (Objects.isNull(path)) {
            throw new IllegalArgumentException();
        }

        this.directory = path;

        return this;
    }

    @Override
    public TrackerBuilder setRemote(String host, String token, String secret) {
        if (Objects.isNull(host) || Objects.isNull(token) || Objects.isNull(secret)) {
            throw new IllegalArgumentException();
        }

        this.host = host;
        this.token = token;
        this.secret = secret;

        return this;
    }

    @Override
    public EventTracker build(String app) {
        WombyteBufferArgs args = new WombyteBufferArgs(directory, host, token, secret);
        WombyteBuffer buffer = buffers.computeIfAbsent(args, WombyteTrackerBuilder::createBuffer);

        return new WombyteTracker(buffer, app);
    }

    private static WombyteBuffer createBuffer(WombyteBufferArgs args) {
        try {

            File directory = new File(args.directory + "/" + args.host);
            Files.createDirectories(directory.toPath());

            return new WombyteBuffer(directory,
                    new WombyteHttpClient(args.host, args.token, args.secret));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
