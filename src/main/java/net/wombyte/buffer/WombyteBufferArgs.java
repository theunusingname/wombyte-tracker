package net.wombyte.buffer;

import java.util.Objects;

public class WombyteBufferArgs {

    public final String directory;
    public final String host;
    public final String token;
    public final String secret;

    public WombyteBufferArgs(String directory, String host, String token, String secret) {
        this.directory = directory;
        this.host = host;
        this.token = token;
        this.secret = secret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WombyteBufferArgs that = (WombyteBufferArgs) o;
        return Objects.equals(directory, that.directory) &&
                Objects.equals(host, that.host);
    }

    @Override
    public int hashCode() {
        return Objects.hash(directory, host);
    }
}
