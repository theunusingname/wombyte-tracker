package net.wombyte.buffer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

public class WombyteBufferWriter implements Closeable {
    private final static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
    }

    private Writer writer;
    private final String file;

    WombyteBufferWriter(String file) {
        this.file = file;
    }

    void write(Object[] records) {
        try {
            Writer lazyWriter = getWriter();

            mapper.writeValue(lazyWriter, records);

            lazyWriter.write("\n");
            lazyWriter.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            if (writer != null) {
                writer.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Writer getWriter() {
        try {
            if (writer == null) {
                writer = new BufferedWriter(new FileWriter(file, true));
            }

            return writer;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
