package net.wombyte.buffer;

import net.wombyte.WombyteHttpClient;
import net.wombyte.event.WombyteEventContext;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class WombyteBuffer implements Closeable {

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private final File directory;
    private final WombyteHttpClient client;

    private final AtomicReference<WombyteBufferWriter> writer;

    public WombyteBuffer(File directory, WombyteHttpClient client) {
        this.directory = directory;
        this.client = client;

        this.writer = new AtomicReference<>(createWriter());
    }

    private WombyteBufferWriter createWriter() {
        return new WombyteBufferWriter(directory.toString() + "/" + UUID.randomUUID());
    }

    private File[] rotate() {
        File[] files = directory.listFiles();

        writer.getAndSet(createWriter())
                .close();

        return files;
    }

    public CompletableFuture<Boolean> append(WombyteEventContext context, Map<String, Object> attributes) {
        Object[] record = new Object[]
        {
            context.timestamp,
            context.id,
            context.event,
            context.app,

            attributes
        };

        return CompletableFuture.supplyAsync(() -> {
            lock.writeLock().lock();

            try {
                writer.get().write(record);
            } finally {
                lock.writeLock().unlock();
            }

            return true;
        });

    }

    @Override
    public void close() {
        writer.get().close();
    }

    public void flushLines() {
        for (File file : rotate()) {
            lock.readLock().lock();

            try {
                if (client.sendFile(file) < 500){
                    Files.delete(file.toPath());
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                lock.readLock().unlock();
            }
        }
    }


}
