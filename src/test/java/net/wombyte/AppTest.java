package net.wombyte;

import com.sun.net.httpserver.HttpServer;
import net.wombyte.tracker.WombyteTracker;
import net.wombyte.tracker.WombyteTrackerBuilder;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;


public class AppTest {
    private static List<String> payload = new ArrayList<>();
    private static HttpServer server;

    @BeforeClass
    public static void setUp() throws IOException {
        server = HttpServer.create();
        server.bind(new InetSocketAddress(80), 10);
        server.createContext("/", httpExchange -> {
            Scanner scanner = new Scanner(httpExchange.getRequestBody());
            while (scanner.hasNext()) {
                payload.add(scanner.nextLine());
            }
            httpExchange.sendResponseHeaders(200, 0);
        });
        server.start();
    }

    @Test
    public void testSimpleTrack() throws InterruptedException, ExecutionException {
        payload.clear();
        WombyteTracker tracker = (WombyteTracker) WombyteTrackerBuilder.create()
                .setLogDirectory(".env")
                .setRemote("localhost", "test", "test")
                .build("test");

        tracker.track("java")
                .addNumeral("foo", 1.23)
                .addString("bar", "1234")
                .send()
                .get();

        tracker.flush().get();

        payload.forEach(obj -> {
            try {
                System.out.println(new JSONParser().parse(obj));
            } catch (ParseException e) {
                e.printStackTrace();
                throw new AssertionError();
            }
        });

        Assertions.assertTrue(payload.size() > 0);
        Assertions.assertEquals(0, Objects.requireNonNull(new File(".env/localhost").listFiles()).length);
    }

    @Test
    public void serverUnreachable() throws ExecutionException, InterruptedException, IOException {
        payload.clear();
        try {
            for (File file : Objects.requireNonNull(new File(".env/badlink").listFiles())) {
                Files.delete(file.toPath());
            }
            Files.delete(Paths.get(".env/badlink"));
        } catch (NullPointerException | NoSuchFileException ignored) {
        }

        WombyteTracker tracker = (WombyteTracker) WombyteTrackerBuilder.create()
                .setLogDirectory(".env")
                .setRemote("badlink", "test", "test")
                .build("test");

        tracker.track("java")
                .addNumeral("foo", 1.23)
                .addString("bar", "1234")
                .send()
                .get();

        try {
            tracker.flush().get();
        } catch (ExecutionException e) {
            Assertions.assertEquals(e.getCause().getCause().getLocalizedMessage(), "badlink");
        }

        Assertions.assertTrue(Objects.requireNonNull(new File(".env/badlink").listFiles()).length > 0);
    }

}
